import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth  } from '@angular/fire/auth';
import { AuthService } from '../auth.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  email: string;
  password: string;
  name: string;
  code = '';
  message = '';
  required = '';
  require = true;
  flag = false;
  specialChar = ['!','@','#','$','%','^','&','*',
                  '(',')','_','-','+','=',"'",'"',
                  ',','/','?','.','>','<','{','}',
                  '[',']','|','~','`',':',';']
  valid = false;
  validation = "";
  hide = true;

  signUp()
  {
    this.flag = false;
    this.valid = false;
    this.require = true;

    if (this.name == null || this.password == null || this.email == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  if(this.require)
    {
      for (let char of this.specialChar)
      {
        if (this.password.includes(char))
        {
          this.valid = true;
        }
      }
    }
     if (this.valid && this.require)
    {
      console.log("true;");
      this.authService.signUp(this.email,this.password)
        .then(value => { 
              this.authService.updateProfile(value.user,this.name);
              this.authService.addUser(value.user,this.name, this.email); 
        }).then(value =>{
          this.router.navigate(['/welcome']);
        }).catch(err => {
          this.flag = true;
          this.code = err.code;
          this.message = err.message;
          console.log(err);
        })
    }
    else
    {
      this.validation = "Password must contain special characters";
    }
  }

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

}
